require('dotenv').config()
const https = require('https')
const { Kafka } = require('kafkajs')

// config target
const kafka = new Kafka({
  clientId: process.env.CLIENTID,
  brokers: process.env.BROKERS.split(',')
})

const producer = kafka.producer()
await producer.connect()

// config source
const options = {
  hostname: process.env.HOSTNAME,
  port: process.env.PORT,
  path: process.env.PATH,
  method: process.env.METHOD
}

//infinite loop
while(1){
    //set looping
    await new Promise(resolve => setTimeout(resolve, process.env.SLEEP));
    //fire requests
    const req = https.request(options, res => {
        console.log(`statusCode: ${res.statusCode}`)    
        // send to kafka
        res.on('data', d => {
        //process.stdout.write(d)
        await producer.send({
            topic: process.env.TOPIC,
            messages: d.split["\n"],
          })          
        })
    })  

  //source error  
  req.on('error', error => {
    console.error(error)
  })  
  req.end()  
}
